#!/bin/bash

echo -e $YELLOW"User to invite :"$WHITE
read INVITEE

echo -e $YELLOW"Password:"$WHITE
read -s PWD

REP=`curl --silent --user $USER:$PWD" --request POST https://bitbucket.org/api/1.0/invitations/$USER/conf/$INVITEE --data permission=write`
if [[ `echo $REP | grep "invalid email address supplied"` ]];then
    echo "Bad email"
fi

